import re

def parse_string(string: str, rates: dict) -> dict:
    string = string.upper()
    string_words = re.findall(r'[A-Z]+', string)
    currencies = [x for x in rates]
    currency_from_to = [x for x in string_words if x in currencies]

    output = {
        'amount': 0,
        'from_currency': '',
        'to_currency': ''
    }

    if number := re.search(r'\d+', string):
        output['amount'] = number.group(0)
    else:
        raise Exception('No value was given!')

    if len(currency_from_to) == 2: 
        output['from_currency'] = currency_from_to[0]
        output['to_currency'] = currency_from_to[1]
    else:
        raise Exception('Incorrect amount of currencies given, expected 2 got {}'.format(
            len(currency_from_to)
        ))

    return output

# TODO: stop using manual testing
if __name__ == '__main__':
    test = ['500nok dkk','200 dkk to nok']
    import json
    json_data = json.loads("""{"rates":{"CAD":1.5644,"HKD":9.1236,"ISK":161.1,"PHP":57.631,"DKK":7.4472,"HUF":345.15,"CZK":26.117,"AUD":1.6514,"RON":4.8356,"SEK":10.2513,"IDR":17374.0,"INR":88.051,"BRL":6.3782,"RUB":86.3013,"HRK":7.4885,"JPY":125.92,"THB":36.62,"CHF":1.0755,"SGD":1.6166,"PLN":4.4033,"BGN":1.9558,"TRY":8.6074,"CNY":8.1758,"NOK":10.5463,"NZD":1.7997,"ZAR":20.5097,"USD":1.1771,"MXN":26.3381,"ILS":4.0101,"GBP":0.90475,"KRW":1393.92,"MYR":4.9362},"base":"EUR","date":"2020-08-12"}""")
    rates = json_data['rates']
    for item in test:
        print(item, parse_string(item, rates))

