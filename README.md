# Cashconv
```
$ cashconv.py --help
Usage: cashconv.py [OPTIONS] [TEXT]...

  Cashconv: Currency conversions in your terminal.

     Speedy, possible to run offline and with minimal API calls.

     Usage:     cashconv.py 100EUR to USD     cashconv.py 200 JPY GBP

Options:
  -l, --listall  Show available currencies.
  -v, --verbose  Show debug output
  --version      Show the version and exit.
  --help         Show this message and exit.
$ cashconv.py 100eur to nok
100.00 EUR → 1061.88 NOK
$ _
```
Speedy, possible to run offline and with minimal API calls

Data fetched from [exchangeratesapi.io](https://exchangeratesapi.io/)'s free API.
