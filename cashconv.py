#!/usr/bin/env python3

import os.path
import click
from datetime import datetime, timedelta, timezone
import logging

import web_handler
import json_handler
import input_handler


API_URL = 'https://api.exchangeratesapi.io/latest?base=EUR'
JSON_FILE = 'currency.json'
VERSION = '1.1.0'


def convert_from_to(json_data: dict, input_dict: dict) -> str:
    amount = float(input_dict['amount'])
    from_currency = input_dict['from_currency']
    to_currency = input_dict['to_currency']

    converted_amount = (amount / json_data['rates'][from_currency]) * json_data['rates'][to_currency]
    
    return ' '.join(["{:.2f}".format(amount), str(from_currency), '→',
                     "{:.2f}".format(converted_amount), str(to_currency)])

def json_update_is_needed(json_data: dict) -> bool:
    now = datetime.now(tz=timezone.utc)
    yesterday = now - timedelta(days=1)

    logging.info('UPDATE_CHECK Checking if JSON is out of date…')
    if json_data['last_pull'] != now.strftime('%Y-%m-%d'):
        # Server updates slowly, this next if statement avoids sending
        # unnecessary requests and allow some extra time for
        # the server to update its entries.
        if ((json_data['date'] == yesterday.strftime('%Y-%m-%d') and
                now.strftime('%H:%M:%S') > '09:00:00') or
                (json_data['date'] != yesterday.strftime('%Y-%m-%d')) ):
            logging.info('UPDATE_CHECK JSON is out of date. Current date: {} while JSON date: {}'.format(
                now.strftime("%Y-%m-%d"), json_data["date"]))
            return True
    logging.info('UPDATE_CHECK JSON is up to date.')
    return False

def json_update_file_from_url(filename: str, url: str, important: bool = False) -> None:
    response = web_handler.fetch_data_from_url(url, important=important)
    if response:
        json_data = json_handler.json_from_str(response)
        json_data = json_handler.add_base_currency_to_json(json_data)
        json_data = json_handler.add_last_pull_date_to_json(json_data)
        json_handler.store_json_to_file(json_data, filename)
    elif not important:
        logging.warning('JSON_UPDATE Failed to update JSON file.')
    else:
        raise

@click.command()
@click.argument('text', nargs=-1)
@click.option('-l', '--listall', is_flag=True, help="Show available currencies.")
@click.option('-v', '--verbose', count=True, help="Show debug output")
@click.version_option(VERSION)
def main(text: tuple, listall: bool, verbose: int):
    """Cashconv: Currency conversions in your terminal.

    Speedy, possible to run offline and with minimal API calls.

    Usage:
    \n cashconv.py 100EUR to USD
    \n cashconv.py 200 JPY GBP
    """
    if verbose == 0:
        logging.basicConfig(level=logging.WARNING)
    if verbose == 1:
        logging.basicConfig(level=logging.INFO)
    elif verbose > 1:
        logging.basicConfig(level=logging.DEBUG)


    if not os.path.isfile(JSON_FILE):
        print('Creating database…')
        json_update_file_from_url(JSON_FILE, API_URL, important=True)

    json_data = json_handler.load_json_from_file(JSON_FILE)

    if json_update_is_needed(json_data):
        print('Updating database…')
        json_update_file_from_url(JSON_FILE, API_URL)
        json_data = json_handler.load_json_from_file(JSON_FILE)


    if listall:
        print(', '.join(list(json_data['rates'].keys())))
    else:
        if not text:
            user_input = input('> ')
        else:
            user_input = ' '.join(text)

        input_dict = None
        while input_dict is None:
            try:
                input_dict = input_handler.parse_string(user_input, json_data['rates'])
            except: # TODO: Check for thrown exepctions and give a better error message.
                print('That didnt work, try again.')
                user_input = input('> ')
        print(convert_from_to(json_data, input_dict))

if __name__ == "__main__":
    main()
