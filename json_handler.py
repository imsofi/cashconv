import json
import logging
from datetime import datetime, timezone


def json_from_str(data: str) -> dict:
    """Takes in a UTF-8 string with json and parses it to a dict."""
    try:
        parsed_json = json.loads(data)
    except json.JSONDecodeError as e:
        logging.error(f'JSON JSONDecodeError: {e.msg} at {e.pos} in {e.doc}')
        raise
    else:
        logging.debug(f'JSON Parsed string to json: {parsed_json}')
        return parsed_json


def store_json_to_file(json_dict: dict, filename: str) -> None:
    """Stores json to specified file."""
    logging.info(f'JSON Storing json to file: {filename}')
    with open(filename, 'w') as file:
        json.dump(json_dict, file, indent=4)


def load_json_from_file(filename: str) -> dict:
    """Loads json from the specified file."""
    logging.info(f'JSON Loading json from file: {filename}')
    with open(filename, 'r') as file:
        return json.load(file)


def add_base_currency_to_json(json_dict: dict) -> dict:
    base_currency = json_dict['base']
    logging.info(f'JSON Adding base currency {repr(base_currency)} to json.')
    json_dict['rates'][base_currency] = 1.0
    return json_dict


def add_last_pull_date_to_json(json_dict: dict) -> dict:
    now = datetime.now(tz=timezone.utc)
    last_pull_date = now.strftime('%Y-%m-%d')
    logging.info(f'JSON Adding last pull date {last_pull_date}')
    json_dict['last_pull'] = last_pull_date
    return json_dict


# TODO: Manual unit testing
if __name__ == '__main__':
    from pprint import pprint
    example_json="""{"rates":{"CAD":1.5644,"HKD":9.1236,"ISK":161.1,"PHP":57.631,"DKK":7.4472,"HUF":345.15,"CZK":26.117,"AUD":1.6514,"RON":4.8356,"SEK":10.2513,"IDR":17374.0,"INR":88.051,"BRL":6.3782,"RUB":86.3013,"HRK":7.4885,"JPY":125.92,"THB":36.62,"CHF":1.0755,"SGD":1.6166,"PLN":4.4033,"BGN":1.9558,"TRY":8.6074,"CNY":8.1758,"NOK":10.5463,"NZD":1.7997,"ZAR":20.5097,"USD":1.1771,"MXN":26.3381,"ILS":4.0101,"GBP":0.90475,"KRW":1393.92,"MYR":4.9362},"base":"EUR","date":"2020-08-12"}"""
    json_data = json_from_str(example_json)
    pprint(json_data)
    json_data = add_base_currency_to_json(json_data)
    store_json_to_file(json_data,'my.json')
    json_data2 = load_json_from_file('my.json')
    if json_data == json_data2:
        pprint(json_data2)

