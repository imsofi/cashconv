import urllib.request
import urllib.error
import logging
import json
from typing import Union, cast, Any


def fetch_data_from_url(url: str, important: bool = False) -> Union[str, None]:
    """Gets UTF-8 data from spesified url."""
    logging.info(f'WEB Fetching data from: {url}')
    req = urllib.request.Request(url)
    try:
        response = urllib.request.urlopen(req)
    except (urllib.error.HTTPError, urllib.error.URLError) as e:
        if hasattr(e, 'reason'):
            logging.error(f'WEB URLError: {cast(Any, e).reason} for {url}')
        elif hasattr(e, 'code'):
            logging.error(f'WEB HTTPError: {cast(Any, e).code} for {url}')
        if important:
            raise
        else:
            return None
    else:
        logging.info(f'WEB Successfully fetched data from: {url}')
        return response.read().decode('utf-8')


# TODO: Manually done unit testing
if __name__ == '__main__':
    output = fetch_data_from_url('https://api.exchangeratesapi.io/latest')
    print(repr(output))
